package main

import (
	"fmt"

	"paru.net/gosimpleapp/internal/app"
)

func main() {
	fmt.Println("this is main...")
	app.Start()
}
