# Golang simple app

This can be a base structure for golang module or simple app

└── cmd
    └── main.go
└── config
    └── app_config.yaml
└── internal
    └── app
        └── app.go
    └── pkg?
    └── services
└── vendor

